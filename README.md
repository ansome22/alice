# Introduction

A fan game based on [Americans wiki](https://www.americanmcgee.com/asylumwiki/index.php?title=Main_Page)

# What currently is here

## HUD

- Simple health
- Simple Time bar
- Coin/Money counter
- Game instructions
- Subtitle bar
- Redial Menu (Circle Switching Weapons)

## Movement

- RUN SPEED
- JUMPING
  - Double Jumping
  - Hold to glide
- Jump Pads
- Swimming
  - Dashing
- Crouching
- Ledge Climbing

## Abilities

All Abilities can be used in dev room

- Slow down time
- Shrink
- Simple Climbing
- Manipulate Gravity for Secret Garden Maze
- Switch Camera (First Person, Third Person)
- Rope Swinging

## Maps

- Hatters Domain
- Prelude
- Start on Depression
- Secret Garden Maze
- Painted World
- Loading screen (Quotes)

## Dresses

- Logic is ready to switch on level or user choice

## Powerups

- Logic and simple models (money, health and time)

## Puzzles

- Time puzzle (Rat cage)
- Push-able Objects

## Weapons

- Timebomb

  - Explodes after a time
  - Harms enemy
  - Destroys destructible objects

- Toad Top

  - Projectile

- Flamingo

  - Melee
  - Projectile

- Grapple Hook

# Developer Side

- Objects follow a path
- Destructible
- Mushrooms
- Configurable gravity
- Gave an area or world a painting look
- Heat Haze
- Colourful Fog

# Enemies

- Walk to random locations or a predetermined route
- React To sounds
- Melee
- Shoot Projectiles

# Sound and music

- Play music in an area or play music following player.
- Dialog in an area

# Missing

- Water

  - Visual for transitioning to swimming water to walking water

- Sliding

* Powerups

  - Emotional vials

* Enemies / Combat

- Rope Walking

- Map

  https://old.reddit.com/r/unrealengine/comments/4ud7b0/realtime_dynamic_holographic_map/

- Melee Effects

  https://www.youtube.com/watch?v=nwndo13A8sc&list=PLwMiBtF6WzsqXWJF-pjjkovVZM-04lzt_&index=5

- Paint Weapon

https://old.reddit.com/r/unrealengine/comments/9ti3io/recursive_pain_paint_mechanics/

- Environment Query System

- Panini Projection (FOV)

- Player Death

  - Destruction
  - Frozen
  - Fire
  - Flatten

- Conversation
  https://www.youtube.com/watch?v=nTXB_vdXg2M

Last Known Location
https://github.com/RohitKotiveetil/UnrealEngine--LastKnownPositionVisualization

- Stealth example
  https://old.reddit.com/r/unrealengine/comments/9iyfqo/wip_thirdperson_stealth_archery_prototype/

- Main menu
  https://www.youtube.com/watch?v=tp1Yp4HbNSE

Radial Post-Process Mask
https://www.youtube.com/watch?v=ABfp5dHSFSo&list=PLAWAqMERLKYvGevBf6iWhzdAv378ZxH7L&index=15

Footsteps
https://www.youtube.com/watch?v=LPtDqsMFFys&list=PLAWAqMERLKYvGevBf6iWhzdAv378ZxH7L&index=23

Wind Direction
https://www.youtube.com/watch?v=Nowz09Y-QZw&list=PLAWAqMERLKYvGevBf6iWhzdAv378ZxH7L&index=37

Spray Paint
https://www.youtube.com/watch?v=fIidTM_4Yuk&list=PLAWAqMERLKYvGevBf6iWhzdAv378ZxH7L&index=45

Toon Shader
https://www.youtube.com/watch?v=0UBNXneL1oo&list=PLAWAqMERLKYvGevBf6iWhzdAv378ZxH7L&index=52

Cloth
https://www.youtube.com/watch?v=kjOq8OB_3AQ&list=PLAWAqMERLKYvGevBf6iWhzdAv378ZxH7L&index=54

Projector
https://www.youtube.com/watch?v=4pXfKjlC4BU&list=PLAWAqMERLKYvGevBf6iWhzdAv378ZxH7L&index=56

Loop through images one with a door, if the player stops on the frame a door appears

Gravity
https://www.youtube.com/watch?v=8CIvJnhGTe8

Light-weight IK solver (similar to FABRIK) typically used to drive a chain of bones
https://docs.unrealengine.com/en-US/Engine/Animation/NodeReference/SkeletalControls/CCDIK/index.html

Look At
https://docs.unrealengine.com/en-US/Engine/Animation/NodeReference/SkeletalControls/LookAt/index.html

UMG Rich Text Block Widget (Controls)
https://docs.unrealengine.com/en-US/Support/Builds/ReleaseNotes/4_20/#new:umgrichtextblockwidget

## Jacks (Simple)

- The jacks will float in the air and will be able to be picked by Alice and gathered to use for Stealth movements. Also will be upgraded later to an actual weapon.
  - Projectile
  - Hovering to pick up

Puzzle
Collect Pieces of a door (Wood,door handle, key) to go further

## Enemy

Twisted experiments of patients

Baking soda to grow things

## Puzzle

https://old.reddit.com/r/Unity3D/comments/b4mn1l/you_guys_were_super_supportive_of_my_shader/

## Ai Fast moving

https://old.reddit.com/r/Unity3D/comments/bqrcqz/first_iteration_of_ai_combat_for_my_game/

## Shaders

https://old.reddit.com/user/Couch_Game_Crafters?sort=top

## Googly Eyes

https://twitter.com/i/status/1156981961547816960

## Animal Ears

https://twitter.com/i/status/1139303619407306752

## Chaos

https://twitter.com/i/status/1119617216213860352

https://www.youtube.com/watch?v=JFFs0-Laf5A

https://www.youtube.com/watch?v=BY7qgXW8se8

## water interaction

https://twitter.com/i/status/977512367927021568

## Void Grasp

https://twitter.com/WoodstockPro/status/1159726330277183488?s=20

## Chest Spawn

https://twitter.com/Foxician/status/1159163578336890880?s=20

## Muzzle Flashes

https://www.artstation.com/artwork/W29ybE

## RAGE

https://twitter.com/jordy_j_s/status/1159886653839892480

## Parallax

https://twitter.com/tntcproject/status/1152189499985793026

## Image in Crystals

https://twitter.com/damjanmx/status/1157260632200232963

## Melee

https://www.artstation.com/artwork/xzeeWE

Dust
https://twitter.com/sarahcarmody/status/1092194879684337666

## Ancient

https://sketchfab.com/3d-models/the-inner-sanctum-5e132c1a345a44aa8c3d2a50b38e2ae7

## Characters

https://sketchfab.com/3d-models/magic-merchant-cat-6bf54e8e09984ea3a2d266f53dd69442

https://sketchfab.com/3d-models/wizard-cat-50d08930b5c8459e8dcf7f53ac31fd8c

https://sketchfab.com/3d-models/alice-3ffefa6fd61d44589d87bd9a6fcf29b0

https://sketchfab.com/3d-models/on-a-mission-54ed807a9b334036824d4f869da068ae

https://sketchfab.com/3d-models/mushroom-golem-ca813249ffb84a789212fdc293fdbbd7

https://sketchfab.com/3d-models/the-secret-of-nimh-the-great-owl-8225d4c1fcd44891b5536b441b350cb1

https://sketchfab.com/3d-models/old-wizard-33b508c86aac4b2d993cadff1045aa78

https://sketchfab.com/3d-models/the-goaty-your-best-friend-b93549550863427989cbf1bb11b52490

https://sketchfab.com/3d-models/woodhouse-b3a2dceebb554638802ab421ea42ee3a

## Vorpal Blade

https://sketchfab.com/3d-models/vorpal-blade-146ab0fe982c402a980a7ae4e4267851

## Items

https://sketchfab.com/3d-models/demonic-potion-b6f3ddf19cda405ea5f6bce45fcdb826

# Companion

- Knows where the player is
- Knows the goal points of a map

- Two Points of interest

  - Lesser points (Sitting down, vending machines)
  - Golden moments (Narrative moments, single moments)

- Characters are different behaviours when different events occur
  - Fight enemies or run away from them.
  - Helping player (Giving items to player)
  - Interreacting with objects (Safes, doors)
  - Noticing dead bodies

## Emotional States

- Companion to be believable must have multiple emotional states (Happy, upset, angry, sad).
- This also includes developing multiple animations for each emotion.
- Points of interest will also be seen through different emotions
